
from PyQt5.QtWidgets import QMainWindow, QGridLayout, QWidget
from PyQt5.QtCore import QSize, Qt
from PyQt5.QtWidgets import QPushButton, QMessageBox, QLineEdit
from PyQt5.QtGui import QIcon
from calculator import Calculator

class MainWindow(QMainWindow):
    def __init__(self):
        QMainWindow.__init__(self)

        self.setMinimumSize(QSize(200, 250)) # wielkość okna
        self.setWindowTitle("Better Calculator") # nazwa okna
        self.setWindowIcon(QIcon('resources/kalkulator.png'))

        sentence = QWidget(self)
        self.setCentralWidget(sentence)

        grid_layout = QGridLayout(self) #przypis kształtu siatki
        sentence.setLayout(grid_layout)

        self.interface_config()
        self.number=""
        self.Calculator = Calculator()

    def interface_config(self):
        x_size=50
        y_size=50
        line_start_point=0
        col_start_point=0

        self.keyboard_config(x_size,y_size,line_start_point,col_start_point)
        self.display_config(x_size,y_size,line_start_point,col_start_point)


    def keyboard_config(self, x_size,y_size,line_start_point,col_start_point):
        st_line=line_start_point+y_size
        nd_line=st_line+y_size
        th_line=nd_line+y_size
        four_line=th_line+y_size

        st_col=col_start_point
        nd_col=st_col+x_size
        th_col=nd_col+x_size
        four_col=th_col+x_size

        one = QPushButton('1', self)
        one.resize(x_size, y_size)
        one.move(st_col, st_line)
        one.clicked.connect(self.clicked_method)

        two = QPushButton('2', self)
        two.resize(x_size, y_size)
        two.move(nd_col, st_line)
        two.clicked.connect(self.clicked_method)

        three = QPushButton('3', self)
        three.resize(x_size, y_size)
        three.move(th_col, st_line)
        three.clicked.connect(self.clicked_method)

        four = QPushButton('4', self)
        four.resize(x_size, y_size)
        four.move(st_col, nd_line)
        four.clicked.connect(self.clicked_method)

        five = QPushButton('5', self)
        five.resize(x_size, y_size)
        five.move(nd_col, nd_line)
        five.clicked.connect(self.clicked_method)

        six = QPushButton('6', self)
        six.resize(x_size, y_size)
        six.move(th_col, nd_line)
        six.clicked.connect(self.clicked_method)

        seven = QPushButton('7', self)
        seven.resize(x_size, y_size)
        seven.move(st_col, th_line)
        seven.clicked.connect(self.clicked_method)

        eight = QPushButton('8', self)
        eight.resize(x_size, y_size)
        eight.move(nd_col, th_line)
        eight.clicked.connect(self.clicked_method)

        nine = QPushButton('9', self)
        nine.resize(x_size, y_size)
        nine.move(th_col, th_line)
        nine.clicked.connect(self.clicked_method)

        zero = QPushButton('0',self)
        zero.resize(x_size, y_size)
        zero.move(nd_col, four_line)
        zero.clicked.connect(self.clicked_method)

        equal = QPushButton('=', self)
        equal.resize(x_size, y_size)
        equal.move(th_col, four_line)
        equal.clicked.connect(self.clicked_method)

        plus = QPushButton('+',self)
        plus.resize(x_size, y_size)
        plus.move(four_col,four_line)
        plus.clicked.connect(self.clicked_method)

        dot = QPushButton('.', self)
        dot.resize(x_size, y_size)
        dot.move(st_col, four_line)
        dot.clicked.connect(self.clicked_method)

        minus = QPushButton('-', self)
        minus.resize(x_size, y_size)
        minus.move(four_col, th_line)
        minus.clicked.connect(self.clicked_method)

        multi = QPushButton('*', self)
        multi.resize(x_size, y_size)
        multi.move(four_col, nd_line)
        multi.clicked.connect(self.clicked_method)

        minus = QPushButton('/', self)
        minus.resize(x_size, y_size)
        minus.move(four_col, st_line)
        minus.clicked.connect(self.clicked_method)

        clear = QPushButton('C', self)
        clear.resize(x_size, y_size)
        clear.move(four_col, st_line-y_size)
        clear.clicked.connect(self.clicked_method)

    def clicked_method(self):
        clicked_button = self.sender().text()
        if clicked_button == "C":
            self.Calculator.clear()
        elif clicked_button == "=":
            self.Calculator.calculate()
        else:
            self.Calculator.add_sign(clicked_button)
        self.display.setText(str(self.Calculator))

    def closeEvent(self, event):
        window_name="Exit"
        message = "Are you shure you want to exit aplication"
        answer = QMessageBox.question(self, window_name, message, QMessageBox.Yes | QMessageBox.No, QMessageBox.No)
        if answer == QMessageBox.Yes:
            event.accept()
        else:
            event.ignore()

    def f4_detection(self, event):
        if event.key() == Qt.Key_F4:
            return True

    def close_detection(self,event):
        if (event.key() == Qt.Key_ALT) & (self.f4_detection(event)):
            QMessageBox.about(self, "EXIT", "Are you shure you want to exit ?")
            self.close()

    def display_config(self, x_size,y_size,line_start_point,col_start_point):
        self.display = QLineEdit(self)
        self.display.readonly = True
        self.display.move(col_start_point, line_start_point)
        self.display.resize(3*x_size, y_size)

