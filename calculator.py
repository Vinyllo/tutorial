BLANK = " " # one blank space to avoid error in add sign if statement [-1]
from PyQt5.QtWidgets import QMessageBox

class Calculator:
	def __init__(self):
		self.expression = BLANK

	def add_sign(self,chosen):
		if self.expression[-1] in "+-/*" and chosen in "+-/*":
			self.__overwrite_last_sign(chosen)
		else:
			self.expression += chosen

	def clear(self):
		self.expression=BLANK

	def calculate(self):
		if self.expression == BLANK:
			return
		if self.expression[-1] in "+-/*":
			self.__overwrite_last_sign("")
		try:
			self.expression = " "+str(eval(self.expression))
		except ZeroDivisionError:
			QMessageBox.critical(None, "Błąd", "Nie można dzielić przez zero!") # work ?!
			return

	def __str__(self):
		return str(self.expression)

	def __overwrite_last_sign(self, sign):
		str=list(self.expression)
		str[-1]=sign
		self.expression="".join(str)